//
//  SceneDelegate.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 22.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: Public Properties

    var window: UIWindow?

    // MARK: Lifecycle

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow.init(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = createRootViewController()
        window?.makeKeyAndVisible()
    }

    // MARK: Private Methods

    private func createRootViewController() -> UIViewController {
        let containerViewControllers = ContainerViewController()
        let navigationViewController = UINavigationController(rootViewController: containerViewControllers)
        return navigationViewController
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
}
