//
//  DetailsViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit
import MapKit

// MARK: Constants

private extension ConstantString {
    static let titleRouteButton = "Построить маршрут"
}

private extension ConstantColor {
    static let routeButton = UIColor.systemGreen
}

private struct ConstantSize {
    static let heightRouteButton = 70.0
    static let spacingStackView = 20.0
    static let trailingLeadingOffsetStackView = 10.0
    static let topOffsetStackView = 20.0
}

protocol DetailsDisplayLogic: AnyObject {
    func displaySomething(viewModel: Details.Something.ViewModel)
}

class DetailsViewController: UIViewController, DetailsDisplayLogic {

    // MARK: Public Properties
    var interactor: DetailsBusinessLogic?
    var router: (NSObjectProtocol & DetailsRoutingLogic & DetailsDataPassing)?

    // MARK: Private Properties
    private var locationCoordinate2D = CLLocationCoordinate2D()
    private var detailsBankFacility: [DetailsBankFacility] = []

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = DetailsInteractor()
        let presenter = DetailsPresenter()
        let router = DetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        addSubviews()
        setConstraints()
        doSomething()
    }

    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        view.addSubview(routeButton)
    }

    private func setConstraints() {
        scrollView.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view.safeAreaLayoutGuide)
            make.top.equalToSuperview()
            make.bottom.equalTo(routeButton.snp.top)
        }
        stackView.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view.safeAreaLayoutGuide).offset(ConstantSize.trailingLeadingOffsetStackView)
            make.top.equalToSuperview().offset(ConstantSize.topOffsetStackView)
            make.bottom.equalToSuperview()
        }
        routeButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heightRouteButton)
            make.bottom.equalToSuperview()
        }
    }

    // MARK: Do something

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = ConstantSize.spacingStackView
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var routeButton: UIButton = {
        let routeButton = UIButton()
        routeButton.backgroundColor = ConstantColor.routeButton
        routeButton.setTitle(ConstantString.titleRouteButton, for: .normal)
        routeButton.addTarget(self, action: #selector(openMapForRoute), for: .touchUpInside)
        return routeButton
    }()

    func doSomething() {
        let request = Details.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: Details.Something.ViewModel) {
        detailsBankFacility = viewModel.detailsBankFacility
        locationCoordinate2D = viewModel.coordinate2D
        createDetailList()
    }

    private func createDetailList() {
        for detailBankFacility in detailsBankFacility {
            let cellView = CellView()
            stackView.addArrangedSubview(cellView)
            cellView.titleLable.text = detailBankFacility.title
            cellView.descriptionLable.text = detailBankFacility.description
        }
    }

    @objc private func openMapForRoute() {
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        let placemark = MKPlacemark(coordinate: locationCoordinate2D)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: launchOptions)
    }
}
