//
//  DetailsPresenter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit

protocol DetailsPresentationLogic {
    func presentSomething(response: Details.Something.Response)
}

class DetailsPresenter: DetailsPresentationLogic {
    weak var viewController: DetailsDisplayLogic?

    // MARK: Do something

    func presentSomething(response: Details.Something.Response) {
        let viewModel = Details.Something.ViewModel(detailsBankFacility: response.bankFacility.getDetails(),
                                                    coordinate2D: response.bankFacility.getCoordinate2D())
        viewController?.displaySomething(viewModel: viewModel)
    }
}
