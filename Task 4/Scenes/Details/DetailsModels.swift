//
//  DetailsModels.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit
import CoreLocation

enum Details {
    // MARK: Use cases

    enum Something {
        struct Request {
        }
        struct Response {
            var bankFacility: BankFacility
        }
        struct ViewModel {
            var detailsBankFacility: [DetailsBankFacility]
            var coordinate2D: CLLocationCoordinate2D
        }
    }
}
