//
//  DetailsRouter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit

@objc protocol DetailsRoutingLogic {
}

protocol DetailsDataPassing {
    var dataStore: DetailsDataStore? { get }
}

class DetailsRouter: NSObject, DetailsRoutingLogic, DetailsDataPassing {
    weak var viewController: DetailsViewController?
    var dataStore: DetailsDataStore?
}
