//
//  DetailsInteractor.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit

protocol DetailsBusinessLogic {
    func doSomething(request: Details.Something.Request)
}

protocol DetailsDataStore {
    var bankFacility: BankFacility! { get set }
}

class DetailsInteractor: DetailsBusinessLogic, DetailsDataStore {
    var presenter: DetailsPresentationLogic?
    var worker: DetailsWorker?
    var bankFacility: BankFacility!

    // MARK: Do something

    func doSomething(request: Details.Something.Request) {
        worker = DetailsWorker()
        worker?.doSomeWork()

        let response = Details.Something.Response(bankFacility: bankFacility)
        presenter?.presentSomething(response: response)
    }
}
