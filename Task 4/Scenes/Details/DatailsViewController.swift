//
//  DetailsViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DetailsDisplayLogic: AnyObject {
    func displaySomething(viewModel: Datails.Something.ViewModel)
}

class DatailsViewController: UIViewController, DatailsDisplayLogic {
    var interactor: DatailsBusinessLogic?
    var router: (NSObjectProtocol & DatailsRoutingLogic & DatailsDataPassing)?

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = DatailsInteractor()
        let presenter = DatailsPresenter()
        let router = DatailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        doSomething()
    }

    // MARK: Do something

    func doSomething() {
        let request = Datails.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: Datails.Something.ViewModel) {
    }
}
