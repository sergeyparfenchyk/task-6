//
//  ContainerInteractor.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
//

import UIKit
import CoreData

protocol ContainerBusinessLogic {
    func doSomething(request: Container.Something.Request)
}

protocol ContainerDataStore {
    var bankFacilities: [BankFacility] { get set }
}

class ContainerInteractor: ContainerBusinessLogic, ContainerDataStore {
    lazy var persistentContainer: NSPersistentContainer = {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate!.persistentContainer
    }()

    var presenter: ContainerPresentationLogic?
    var worker: ContainerWorker?
    var bankFacilities: [BankFacility] = []
    private var allBankFacility: [BankFacility] = []

    // MARK: Do something

    func doSomething(request: Container.Something.Request) {
        switch request.type {
        case .update:
            update(request: request)
        case .download:
            download(request: request)
        case .filter:
            filter(request: request)
        }
    }

    private func download(request: Container.Something.Request) {
        worker = ContainerWorker()
        worker?.doSomeWork(persistentContainer: persistentContainer, completion: { result in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    self.allBankFacility = data
                    self.filter(request: request)
                case .failure(let error):
                    print(error)
                }
            }
        })
    }

    private func update(request: Container.Something.Request) {
        worker = ContainerWorker()
        worker?.doSomeWork(persistentContainer: persistentContainer, completion: { result in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    self.allBankFacility = data
                    self.filter(request: request)
                case .failure(let error):
                    print(error)
                }
            }
        })
    }

    private func filter(request: Container.Something.Request) {
        bankFacilities.removeAll()
        for bankFacility in allBankFacility {
            if !request.hide.contains(bankFacility.getType()) {
                bankFacilities.append(bankFacility)
            }
        }
        let response = Container.Something.Response.init(bankFacility: bankFacilities)
        self.presenter?.presentSomething(response: response)
    }
}
