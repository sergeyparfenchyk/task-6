//
//  ContainerRouter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
//

import UIKit

@objc protocol ContainerRoutingLogic {
    func routeToMap(viewController: MapViewController)
    func routeToList(viewController: ListViewController)
}

protocol ContainerDataPassing {
    var dataStore: ContainerDataStore? { get }
}

class ContainerRouter: NSObject, ContainerRoutingLogic, ContainerDataPassing {
    weak var viewController: ContainerViewController?
    var dataStore: ContainerDataStore?

    // MARK: Routing
    func routeToMap(viewController: MapViewController) {
        var destinationDS = viewController.router!.dataStore!
        passDataToMap(source: dataStore!, destination: &destinationDS)
    }
    func routeToList(viewController: ListViewController) {
        var destinationDS = viewController.router!.dataStore!
        passDataToList(source: dataStore!, destination: &destinationDS)
    }

    // MARK: Navigation
    func navigateToMap(source: ContainerViewController, destination: MapViewController) {
      source.show(destination, sender: nil)
    }

    func navigateToList(source: ContainerViewController, destination: ListViewController) {
      source.show(destination, sender: nil)
    }

    // MARK: Passing data
    func passDataToMap(source: ContainerDataStore, destination: inout MapDataStore) {
        destination.bankFacility = source.bankFacilities
    }

    func passDataToList(source: ContainerDataStore, destination: inout ListDataStore) {
        destination.bankFacility = source.bankFacilities
    }
}
