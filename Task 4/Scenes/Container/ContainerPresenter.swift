//
//  ContainerPresenter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
//

import UIKit

protocol ContainerPresentationLogic {
    func presentSomething(response: Container.Something.Response)
}

class ContainerPresenter: ContainerPresentationLogic {
    weak var viewController: ContainerDisplayLogic?

    // MARK: Do something

    func presentSomething(response: Container.Something.Response) {
        let viewModel = Container.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
}
