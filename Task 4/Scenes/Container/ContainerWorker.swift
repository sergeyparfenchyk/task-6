//
//  ContainerWorker.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
//

import UIKit
import CoreData

private extension ConstantString {
    static let entityBranch = "Branch"
    static let entityATM = "ATM"
    static let entityInfobox = "Infobox"
}

class ContainerWorker {
    var bankFacility: [BankFacility] = []

    func doSomeWork(persistentContainer: NSPersistentContainer,
                    completion: @escaping (Result<[BankFacility], Error>) -> Void) {
        let allDataGroup = DispatchGroup()
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchATMResponse(persistentContainer: persistentContainer)
        }
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchBranchResponse(persistentContainer: persistentContainer)
        }
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchInfoBoxResponse(persistentContainer: persistentContainer)
        }
        allDataGroup.notify(queue: DispatchQueue.global()) {[weak self] in
            guard let self = self else { return }
            self.sortBankFacilities()
            completion(.success(self.bankFacility))
        }
    }

    private func sortBankFacilities() {
        bankFacility = bankFacility.sorted { objectFirts, objectSecond in
            if objectFirts.getTownName() < objectSecond.getTownName() {
                return true
            } else if objectFirts.getTownName() > objectSecond.getTownName() {
                return false
            } else {
                let latitude = ConstantNumber.defaultLatitude
                let longitude = ConstantNumber.defaultLongitude
                let firstDistances = sqrt(pow(latitude - objectFirts.getCoordinate2D().latitude, 2) +
                                          (pow(longitude - objectFirts.getCoordinate2D().longitude, 2)))
                let secondDistances = sqrt(pow(latitude - objectSecond.getCoordinate2D().latitude, 2) +
                                           (pow(longitude - objectSecond.getCoordinate2D().longitude, 2)))
                if firstDistances < secondDistances {
                    return true
                }
            }
            return false
        }
    }

    private func fetchATMResponse(persistentContainer: NSPersistentContainer) {
        let context = persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: ConstantString.entityATM,
                                                      in: context) else { return }
        var data = Data()
        let fetchRequest: NSFetchRequest<ATM> = ATM.fetchRequest()
        if let url = ATMResponse.getUrl() {
            do {
                data = try Data(contentsOf: url)
                resetAllRecordsIn(ConstantString.entityATM, context: context)
                let dataATM = NSManagedObject(entity: entity, insertInto: context) as? ATM
                dataATM?.dataJSON = data
                do {
                    try context.save()
                } catch {
                    print(error)
                }
            } catch {
                do {
                    if let someData = try context.fetch(fetchRequest).last?.dataJSON {
                        data = someData
                    }
                } catch {
                    print(error)
                }
            }
            do {
                let atm = try JSONDecoder().decode(ATMResponse.self, from: data)
                bankFacility += atm.data.atm
            } catch {
                print(error)
            }
        }
    }

    private func fetchBranchResponse(persistentContainer: NSPersistentContainer) {
        let context = persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: ConstantString.entityBranch,
                                                      in: context) else { return }
        var data = Data()
        let fetchRequest: NSFetchRequest<Branch> = Branch.fetchRequest()
        if let url = BranchResponse.getUrl() {
            do {
                data = try Data(contentsOf: url)
                resetAllRecordsIn(ConstantString.entityBranch, context: context)
                let dataBranch = NSManagedObject(entity: entity, insertInto: context) as? Branch
                dataBranch?.dataJSON = data
                do {
                    try context.save()
                } catch {
                    print(error)
                }
            } catch {
                do {
                    if let someData = try context.fetch(fetchRequest).last?.dataJSON {
                        data = someData
                    }
                } catch {
                    print(error)
                }
            }
            do {
                let branch = try JSONDecoder().decode(BranchResponse.self, from: data)
                bankFacility += branch.data.branch
            } catch {
                print(error)
            }
        }
    }

    private func fetchInfoBoxResponse(persistentContainer: NSPersistentContainer) {
        let context = persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: ConstantString.entityInfobox,
                                                      in: context) else { return }
        var data = Data()
        let fetchRequest: NSFetchRequest<Infobox> = Infobox.fetchRequest()
        if let url = InfoBoxResponse.getUrl() {
            do {
                data = try Data(contentsOf: url)
                resetAllRecordsIn(ConstantString.entityInfobox, context: context)
                let dataInfobox = NSManagedObject(entity: entity, insertInto: context) as? Infobox
                dataInfobox?.dataJSON = data
                do {
                    try context.save()
                } catch {
                    print(error)
                }
            } catch {
                do {
                    if let someData = try context.fetch(fetchRequest).last?.dataJSON {
                        data = someData
                    }
                } catch {
                    print(error)
                }
            }
            do {
                let infoBox = try JSONDecoder().decode([InfoBoxResponse.DataClass.Infobox].self, from: data)
                bankFacility += infoBox
            } catch {
               print(error)
            }
        }
    }

    private func resetAllRecordsIn(_ entity: String, context: NSManagedObjectContext) {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print(error)
        }
    }
}
