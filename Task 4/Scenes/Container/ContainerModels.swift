//
//  ContainerModels.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
//

import UIKit

enum Container {

    // MARK: Use cases
    enum Something {
        struct Request {
            var type: TypeRequest
            var hide: Set<TypeBankFacility>
        }
        struct Response {
            var bankFacility: [BankFacility]
        }
        struct ViewModel {
        }
    }
}

enum TypeRequest {
    case update
    case download
    case filter
}
