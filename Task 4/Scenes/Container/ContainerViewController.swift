//
//  ContainerViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 11.03.22.
// 

import UIKit
import SnapKit

// MARK: Constants

private extension ConstantString {
    static let atmS = "Беларусбанк"
    static let map = "Карта"
    static let list = "Список"
    static let errorInternetTitle = "Интернет-соединение отсутствует"
    static let errorInternetDescription = "Приложение не работает без доступа к интернету."
    static let errorInternetButtonAllert = "Хорошо"
}

private struct ConstantSize {
    static let heighMapOrListSegmentedControl = 30.0
}

protocol ContainerDisplayLogic: AnyObject {
    func displaySomething(viewModel: Container.Something.ViewModel)
}

class ContainerViewController: UIViewController, ContainerDisplayLogic {

    // MARK: Public Properties
    var interactor: ContainerBusinessLogic?
    var router: (NSObjectProtocol & ContainerRoutingLogic & ContainerDataPassing)?
    private var hideBankFacility: Set<TypeBankFacility> = []

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private lazy var mapViewController = MapViewController()

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = ContainerInteractor()
        let presenter = ContainerPresenter()
        let router = ContainerRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        addSubviews()
        setConstraint()
        doSomething()
        add(child: mapViewController)
        view.bringSubviewToFront(activityIndicatorView)
        setNavigationController()
        activityIndicatorView.startAnimating()
    }

    private func addSubviews() {
        view.addSubview(mapOrListSegmentedControl)
        view.addSubview(activityIndicatorView)
    }

    private func setConstraint() {
        mapOrListSegmentedControl.snp.makeConstraints { make in
            make.height.equalTo(ConstantSize.heighMapOrListSegmentedControl)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
        }
        activityIndicatorView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
    }

    // MARK: Do something

    private func setNavigationController() {
        navigationItem.rightBarButtonItem = updateRightButton
        navigationItem.leftBarButtonItem = filterDataBarButtonItem
        filterDataBarButtonItem.menu = createFilterMenu()
        navigationController?.view.isUserInteractionEnabled = false
        view.isUserInteractionEnabled = false
        updateRightButton.isEnabled = false
    }

    private lazy var updateRightButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                                         target: self,
                                                         action: #selector(updateData))

    private lazy var filterDataBarButtonItem = UIBarButtonItem(systemItem: .edit)
    private lazy var mapAction: UIAction = {
        let mapAction = UIAction(title: ConstantString.map) {[weak self] (_) in
            guard let self = self else { return }
            self.remove(child: self.listViewController)
            self.add(child: self.mapViewController)
            self.router?.routeToMap(viewController: self.mapViewController)
        }
        return mapAction
    }()

    private lazy var listAction: UIAction = {
        let listAction = UIAction(title: ConstantString.list) {[weak self] (_) in
            guard let self = self else { return }
            self.remove(child: self.mapViewController)
            self.add(child: self.listViewController)
            self.router?.routeToList(viewController: self.listViewController)
        }
        return listAction
    }()

    private lazy var listViewController = ListViewController()
    private lazy var mapOrListSegmentedControl: UISegmentedControl = {
        let mapOrListSegmentedControl = UISegmentedControl(frame: .zero,
                                                           actions: [mapAction, listAction])
        mapOrListSegmentedControl.selectedSegmentIndex = 0
        return mapOrListSegmentedControl
    }()

    private lazy var activityIndicatorView = UIActivityIndicatorView(style: .large)

    @objc private func updateData() {
        requestData(type: .update)
        view.bringSubviewToFront(activityIndicatorView)
        activityIndicatorView.startAnimating()
        navigationController?.view.isUserInteractionEnabled = false
        view.isUserInteractionEnabled = false
        updateRightButton.isEnabled = false
    }

    private func createFilterMenu() -> UIMenu {
        let infoBoxAction = UIAction(title: TypeBankFacility.infobox.rawValue,
                                     state: hideBankFacility.contains(.infobox) ? .off : .on) { [weak self] ( _ ) in
            guard let self = self else { return }
            if self.hideBankFacility.remove(.infobox) == nil {
                self.hideBankFacility.insert(.infobox)
            }
            self.requestData(type: .filter)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let atmAction = UIAction(title: TypeBankFacility.atm.rawValue,
                                 state: hideBankFacility.contains(.atm) ? .off : .on) {[weak self] ( _ ) in
            guard let self = self else { return }
            if self.hideBankFacility.remove(.atm) == nil {
                self.hideBankFacility.insert(.atm)
            }
            self.requestData(type: .filter)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let branchBoxAction = UIAction(title: TypeBankFacility.branch.rawValue,
                                       state: hideBankFacility.contains(.branch) ? .off : .on) {[weak self] ( _ ) in
            guard let self = self else { return }
            if self.hideBankFacility.remove(.branch) == nil {
                self.hideBankFacility.insert(.branch)
            }
            self.requestData(type: .filter)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let filterMenuItem = [atmAction, branchBoxAction, infoBoxAction]
        let filteMenu = UIMenu(title: "", image: nil, identifier: nil, options: [], children: filterMenuItem)

        return filteMenu
    }

    private func add(child viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.snp.makeConstraints { make in
            make.top.equalTo(mapOrListSegmentedControl.snp.bottom)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        }
        viewController.didMove(toParent: self)
    }
    private func remove(child viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }

    func doSomething() {
        requestData(type: .download)
    }

    func requestData(type: TypeRequest) {
        let request = Container.Something.Request.init(type: type, hide: hideBankFacility)
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: Container.Something.ViewModel) {
        view.isUserInteractionEnabled = true
        updateRightButton.isEnabled = true
        navigationController?.view.isUserInteractionEnabled = true
        activityIndicatorView.stopAnimating()
        router?.routeToMap(viewController: mapViewController)
        router?.routeToList(viewController: listViewController)
    }
}
