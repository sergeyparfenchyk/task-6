//
//  InfoForСalloutView.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 28.02.22.
//

import UIKit

// MARK: Constants

private extension ConstantString {
    static let moreDetailed = "Подробнее"
}

private extension ConstantColor {
    static let moreDetailedTitle = UIColor.systemGreen
}

private struct ConstantSize {
    static let trailingOffset = 10.0
    static let numberOfLinesPlaceLable = 2
    static let numberOfLinesWorkingHour = 3
    static let closeButton = 20.0
}

class InfoForСalloutView: UIView {

    // MARK: Private Properties

    private(set) lazy var placeLable: UILabel = {
        let placeLable = UILabel()
        placeLable.numberOfLines = ConstantSize.numberOfLinesPlaceLable
        return placeLable
    }()

    private(set) lazy var workingHoursLable: UILabel = {
        let workingHoursLable = UILabel()
        workingHoursLable.numberOfLines = ConstantSize.numberOfLinesWorkingHour
        return workingHoursLable
    }()

    private(set) lazy var currencyLable = UILabel()
    private(set) lazy var cashInLable = UILabel()
    private(set) lazy var index = 0

    private(set) lazy var moreDetailedButton: DetailedButton = {
        let moreDetailedButton = DetailedButton(type: .system)
        moreDetailedButton.setTitle(ConstantString.moreDetailed, for: .normal)
        moreDetailedButton.tintColor = ConstantColor.moreDetailedTitle
        return moreDetailedButton
    }()

    private(set) lazy var closeButton: UIButton = {
        let moreDetailedButton = UIButton(type: .close)
        return moreDetailedButton
    }()

    // MARK: Initialization

    init() {
        super.init(frame: .zero)
        addSubviews()
        setConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Private Methods

    private func addSubviews() {
        addSubview(placeLable)
        addSubview(workingHoursLable)
        addSubview(currencyLable)
        addSubview(cashInLable)
        addSubview(moreDetailedButton)
        addSubview(closeButton)
    }

    private func setConstraint() {
        placeLable.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalTo(closeButton.snp.leading).offset(-ConstantSize.trailingOffset)
            make.top.equalToSuperview()
            make.bottom.equalTo(workingHoursLable.snp.top)
        }

        workingHoursLable.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalTo(closeButton.snp.leading).offset(-ConstantSize.trailingOffset)
            make.top.equalTo(placeLable.snp.bottom)
            make.bottom.equalTo(currencyLable.snp.top)
        }

        currencyLable.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalTo(closeButton.snp.leading).offset(-ConstantSize.trailingOffset)
            make.top.equalTo(workingHoursLable.snp.bottom)
            make.bottom.equalTo(cashInLable.snp.top)
        }

        cashInLable.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalTo(closeButton.snp.leading).offset(-ConstantSize.trailingOffset)
            make.top.equalTo(currencyLable.snp.bottom)
            make.bottom.equalTo(moreDetailedButton.snp.top)
        }

        moreDetailedButton.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.top.equalTo(cashInLable.snp.bottom)
            make.bottom.equalToSuperview()
        }

        closeButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.width.equalTo(ConstantSize.closeButton)
        }
    }
}
