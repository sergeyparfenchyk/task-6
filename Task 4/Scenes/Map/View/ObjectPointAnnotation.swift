//
//  ObjectPointAnnotation.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import UIKit
import MapKit

class ObjectPointAnnotation: MKPointAnnotation {

    // MARK: Public Properties
    var idObject = 0
    var typeObject = TypeBankFacility.atm
}
