//
//  MapInteractor.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import UIKit

protocol MapBusinessLogic {
    func doSomething(request: Map.Something.Request)
}

protocol MapDataStore {
    var bankFacility: [BankFacility] { get set }
}

class MapInteractor: MapBusinessLogic, MapDataStore {
    var bankFacility: [BankFacility] = [] {
        didSet {
            doSomething(request: Map.Something.Request())
        }
    }
    var presenter: MapPresentationLogic?
    var worker: MapWorker?

    // MARK: Do something

    func doSomething(request: Map.Something.Request) {
        worker = MapWorker()
        worker?.doSomeWork()
        let response = Map.Something.Response.init(bankFacility: bankFacility)
        presenter?.presentSomething(response: response)
    }
}
