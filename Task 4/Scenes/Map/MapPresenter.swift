//
//  MapPresenter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import UIKit

protocol MapPresentationLogic {
    func presentSomething(response: Map.Something.Response)
}

class MapPresenter: MapPresentationLogic {
    weak var viewController: MapDisplayLogic?

    // MARK: Do something
    func presentSomething(response: Map.Something.Response) {
        var displayedBankFacilities: [Map.Something.ViewModel.DisplayedBankFacility] = []
        for (index, bankFacility) in response.bankFacility.enumerated() {
            let index = index
            let type = bankFacility.getType()
            let coordinate2D = bankFacility.getCoordinate2D()
            let andressLine = bankFacility.getAndressLine()
            let workTime = bankFacility.getWorkTime()
            let currency = bankFacility.getCurrency()
            let cashIn = bankFacility.getCashIn()
            let displayedBankFacility = Map.Something.ViewModel.DisplayedBankFacility(index: index,
                                                                                      type: type,
                                                                                      coordinate2D: coordinate2D,
                                                                                      andressLine: andressLine,
                                                                                      workTime: workTime,
                                                                                      currency: currency,
                                                                                      cashIn: cashIn)
            displayedBankFacilities.append(displayedBankFacility)
        }
        let viewModel = Map.Something.ViewModel(displayedBankFacilities: displayedBankFacilities)
        viewController?.displaySomething(viewModel: viewModel)
    }
}
