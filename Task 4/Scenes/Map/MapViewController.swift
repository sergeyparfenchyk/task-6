//
//  MapViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import UIKit
import MapKit

// MARK: - Constants

private extension ConstantString {
    static let pointId = "PointId"
    static let locationTitleAlert = "Приложение не знает, где вы находитесь"
    static let locationDescriptionAlert = "Разрешить определять ваше местоположение: " +
                                          "это делается в настройках устройства."
    static let settingButttonAlert = "Настройки"
    static let cancelButttonAlert = "Отменить"
}

protocol MapDisplayLogic: AnyObject {
    func displaySomething(viewModel: Map.Something.ViewModel)
}

class MapViewController: UIViewController, MapDisplayLogic {

    // MARK: Private Properties
    var interactor: MapBusinessLogic?
    var router: (NSObjectProtocol & MapRoutingLogic & MapDataPassing)?
    var displayedBankFacility: [Map.Something.ViewModel.DisplayedBankFacility] = []

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = MapInteractor()
        let presenter = MapPresenter()
        let router = MapRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: Routing

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        doSomething()

        view.addSubview(mapView)
        mapView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            mapView.showsUserLocation = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        switch locationManager.authorizationStatus {
        case .notDetermined:
            break
        case .restricted:
            requestOpenSetting()
        case .denied:
            requestOpenSetting()
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            break
        @unknown default:
            break
        }
    }

    // MARK: Do something

    private let locationManager = CLLocationManager()
    private(set) lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        return mapView
    }()

    func doSomething() {
        let request = Map.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: Map.Something.ViewModel) {
        displayedBankFacility = viewModel.displayedBankFacilities
        addPointInMap(viewModel.displayedBankFacilities)
        if let location = mapView.userLocation.location {
            focusUser(location: location)
        }
    }

    private func focusUser(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                            longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center,
                                        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        mapView.setRegion(region, animated: true)
    }

    private func addPointInMap(_ objectForMap: [Map.Something.ViewModel.DisplayedBankFacility]) {
        mapView.removeAnnotations(mapView.annotations)
        for (index, object) in objectForMap.enumerated() {
            let pin = ObjectPointAnnotation()
            pin.idObject = index
            pin.typeObject = object.type
            if let coordinate2D = object.coordinate2D {
                pin.coordinate = coordinate2D
            }
            mapView.addAnnotation(pin)
        }
    }

    private func requestOpenSetting() {
        let locationAlert = UIAlertController(title: ConstantString.locationTitleAlert,
                                              message: ConstantString.locationDescriptionAlert,
                                              preferredStyle: .alert)
        let settingButton = UIAlertAction(title: ConstantString.settingButttonAlert,
                                          style: .default,
                                          handler: { (_) -> Void in
            self.openSetting()
        })
        let cancelButton = UIAlertAction(title: ConstantString.cancelButttonAlert,
                                         style: .cancel,
                                         handler: nil)
        locationAlert.addAction(settingButton)
        locationAlert.addAction(cancelButton)
        present(locationAlert, animated: true, completion: nil)
    }

    private func openSetting() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString),
           UIApplication.shared.canOpenURL(appSettings) {
            UIApplication.shared.open(appSettings)
        }
    }

    private func createCalloutView(_ dataObjectForMap: Map.Something.ViewModel.DisplayedBankFacility) -> UIView {
        let calloutView = InfoForСalloutView()
        calloutView.placeLable.text = dataObjectForMap.andressLine
        calloutView.workingHoursLable.text = dataObjectForMap.workTime
        calloutView.currencyLable.text = dataObjectForMap.currency
        calloutView.cashInLable.text = "\(ConstantString.serviceTypeCashIn): \(dataObjectForMap.cashIn)"
        calloutView.moreDetailedButton.tag = dataObjectForMap.index
        calloutView.moreDetailedButton.addTarget(self, action: #selector(openDetail), for: .touchUpInside)
        calloutView.closeButton.addTarget(self, action: #selector(hideCallout), for: .touchUpInside)
        return calloutView
    }

    @objc private func hideCallout() {
        mapView.deselectAnnotation(nil, animated: true)
    }

    @objc private func openDetail(button: DetailedButton) {
        let detailsViewController = DetailsViewController()
        detailsViewController.modalPresentationStyle = .pageSheet
        router?.routeToDetails(detailsViewController: detailsViewController)
    }
}

// MARK: MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let objectPointAnnotation = annotation as? ObjectPointAnnotation else { return nil}
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: ConstantString.pointId)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = objectPointAnnotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: ConstantString.pointId)
        }
        let idObject = objectPointAnnotation.idObject
        let typeObject = objectPointAnnotation.typeObject
        view.canShowCallout = true
        var dataObjectForMap: Map.Something.ViewModel.DisplayedBankFacility
        switch typeObject {
        case .atm:
            view.markerTintColor = UIColor.green
        case .infobox:
            view.markerTintColor = UIColor.magenta
        case .branch:
            view.markerTintColor = UIColor.yellow
        }
        dataObjectForMap = displayedBankFacility[idObject]
        let calloutView = createCalloutView(dataObjectForMap)
        view.detailCalloutAccessoryView = calloutView
        return view
    }
}

// MARK: CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
        focusUser(location: userLocation)
    }
}
