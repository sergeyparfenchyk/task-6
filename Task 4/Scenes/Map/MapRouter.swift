//
//  MapRouter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import UIKit

@objc protocol MapRoutingLogic {
    func routeToDetails(detailsViewController: DetailsViewController)
}

protocol MapDataPassing {
    var dataStore: MapDataStore? { get }
}

class MapRouter: NSObject, MapRoutingLogic, MapDataPassing {
    weak var viewController: MapViewController?
    var dataStore: MapDataStore?

    // MARK: Routing

    func routeToDetails(detailsViewController: DetailsViewController) {
        var destinationDS = detailsViewController.router!.dataStore!
        passDataToDetails(source: dataStore!, destination: &destinationDS)
        if let viewController = viewController {
            navigateToSomewhere(source: viewController, destination: detailsViewController)
        }
    }

    // MARK: Navigation

    func navigateToSomewhere(source: MapViewController, destination: DetailsViewController) {
        source.present(destination, animated: true)
    }

    // MARK: Passing data

    func passDataToDetails(source: MapDataStore, destination: inout DetailsDataStore) {
        if let annotation = viewController?.mapView.selectedAnnotations.first as? ObjectPointAnnotation {
            destination.bankFacility = source.bankFacility[annotation.idObject]
        }
    }
}
