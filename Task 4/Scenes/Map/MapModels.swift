//
//  MapModels.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import UIKit
import CoreLocation

enum Map {

  // MARK: Use cases
    enum Something {
        struct Request {
        }
        struct Response {
            var bankFacility: [BankFacility]
        }
        struct ViewModel {
            struct DisplayedBankFacility {
                var index: Int
                var type: TypeBankFacility
                var coordinate2D: CLLocationCoordinate2D?
                var andressLine: String
                var workTime: String
                var currency: String
                var cashIn: String
            }
            var displayedBankFacilities: [DisplayedBankFacility]
        }
    }
}
