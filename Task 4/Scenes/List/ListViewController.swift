//
//  ListViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit
import MapKit

// MARK: Constants

private extension ConstantString {
    static let cellId = "CellId"
    static let sectionId = "SectionId"
}

private struct ConstantSize {
    static let headerSection = 20.0
    static let minimumLineSpacingForSectionAt = 10.0
    static let minimumInteritemSpacingForSectionAt = 10.0
    static let numberRows = 3
    static let heightCell = 150.0
    static let offsetCell = 10.0
}

protocol ListDisplayLogic: AnyObject {
    func displaySomething(viewModel: List.Something.ViewModel)
}

class ListViewController: UIViewController, ListDisplayLogic {

    // Public Properties
    var interactor: ListBusinessLogic?
    var router: (NSObjectProtocol & ListRoutingLogic & ListDataPassing)?

    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup
    private func setup() {
        let viewController = self
        let interactor = ListInteractor()
        let presenter = ListPresenter()
        let router = ListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setContraints()
        doSomething()
    }
    private func addSubviews() {
        view.addSubview(atmCollectionView)
    }

    private func setContraints() {
        atmCollectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
    }

    // MARK: Do something

    private(set) var allObjects: [List.Something.ViewModel.DisplayedBankFacility] = []
    private var cellsInSection = [0]
    private var titlesSection: [String] = []
    private(set) lazy var atmCollectionView: UICollectionView = {
        atmCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        atmCollectionView.register(AtmCollectionViewCell.self, forCellWithReuseIdentifier: ConstantString.cellId)
        atmCollectionView.register(NameCityCollectionReusableView.self,
                                   forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                   withReuseIdentifier: ConstantString.sectionId)
        atmCollectionView.delegate = self
        atmCollectionView.dataSource = self
        return atmCollectionView
    }()

    func doSomething() {
        let request = List.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: List.Something.ViewModel) {
        allObjects = viewModel.displayedBankFacilities
        cellsInSection = viewModel.section.cellsInSection
        titlesSection = viewModel.section.titlesSection
        atmCollectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        let cellInSection = cellsInSection[section]
        return cellInSection
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let sectionCount = cellsInSection.count
        return sectionCount
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if let headerSection = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: ConstantString.sectionId,
            for: indexPath) as? NameCityCollectionReusableView {

            if  titlesSection.count > indexPath.section {
                headerSection.nameCityLable.text = titlesSection[indexPath.section]
            }
            return headerSection
        }
        let headerSection = NameCityCollectionReusableView()
        return headerSection
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: ConstantSize.headerSection)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId,
                                                         for: indexPath) as? AtmCollectionViewCell {
            let item = indexPath.item + indexPath.section
            cell.placeDescriptionLable.text = allObjects[item].adressLine
            cell.workingHoursDescriptionLable.text = allObjects[item].workTime
            cell.currencyDescriptionLable.text = allObjects[item].currency
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId, for: indexPath)
        return cell
    }
}

// MARK: UICollectionViewDelegate

extension ListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsViewController = DetailsViewController()
        detailsViewController.modalPresentationStyle = .pageSheet
        router?.routeToDetails(detailsViewController: detailsViewController)
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension ListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width - ConstantSize.offsetCell *
                              Double((ConstantSize.numberRows - 1))) /
                      Double(ConstantSize.numberRows),
                      height: ConstantSize.heightCell)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumLineSpacingForSectionAt
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumInteritemSpacingForSectionAt
    }
}
