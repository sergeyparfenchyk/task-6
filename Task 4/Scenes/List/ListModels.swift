//
//  ListModels.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit
import CoreLocation

enum List {
    // MARK: Use cases
    enum Something {
        struct Request {
        }
        struct Response {
            var bankFacility: [BankFacility]
        }
        struct ViewModel {
            struct DisplayedBankFacility {
                var index: Int
                var adressLine: String
                var workTime: String
                var currency: String
                var townName: String
            }
            struct Section {
                var cellsInSection: [Int]
                var titlesSection: [String]
            }
            var displayedBankFacilities: [DisplayedBankFacility]
            var section: Section
        }
    }
}
