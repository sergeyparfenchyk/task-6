//
//  ListInteractor.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit

protocol ListBusinessLogic {
    func doSomething(request: List.Something.Request)
}

protocol ListDataStore {
    var bankFacility: [BankFacility] { get set }
}

class ListInteractor: ListBusinessLogic, ListDataStore {
    var bankFacility: [BankFacility] = [] {
        didSet {
            doSomething(request: List.Something.Request())
        }
    }
    var presenter: ListPresentationLogic?
    var worker: ListWorker?

    // MARK: Do something
    func doSomething(request: List.Something.Request) {
        worker = ListWorker()
        worker?.doSomeWork()
        let response = List.Something.Response(bankFacility: bankFacility)
        presenter?.presentSomething(response: response)
    }
}
