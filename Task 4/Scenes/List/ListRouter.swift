//
//  ListRouter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit

@objc protocol ListRoutingLogic {
    func routeToDetails(detailsViewController: DetailsViewController)
}

protocol ListDataPassing {
    var dataStore: ListDataStore? { get }
}

class ListRouter: NSObject, ListRoutingLogic, ListDataPassing {
    weak var viewController: ListViewController?
    var dataStore: ListDataStore?

    // MARK: Routing

    func routeToDetails(detailsViewController: DetailsViewController) {
        var destinationDS = detailsViewController.router!.dataStore!
        passDataToDetails(source: dataStore!, destination: &destinationDS)
        if let viewController = viewController {
            navigateToSomewhere(source: viewController, destination: detailsViewController)
        }
    }

    // MARK: Navigation

    func navigateToSomewhere(source: ListViewController, destination: DetailsViewController) {
        source.present(destination, animated: true)
    }

    // MARK: Passing data

    func passDataToDetails(source: ListDataStore, destination: inout DetailsDataStore) {
        if let row = viewController?.atmCollectionView.indexPathsForSelectedItems?.first?.row,
           let section = viewController?.atmCollectionView.indexPathsForSelectedItems?.first?.section {
            destination.bankFacility = source.bankFacility[row + section]
        }
    }
}
