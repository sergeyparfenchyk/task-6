//
//  ListPresenter.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 13.03.22.
//

import UIKit
import CoreLocation

protocol ListPresentationLogic {
    func presentSomething(response: List.Something.Response)
}

class ListPresenter: NSObject, ListPresentationLogic, CLLocationManagerDelegate {
    weak var viewController: ListDisplayLogic?

    // MARK: Do something
    func presentSomething(response: List.Something.Response) {
        var displayedBankFacilities: [List.Something.ViewModel.DisplayedBankFacility] = []
        for (index, bankFacility) in response.bankFacility.enumerated() {
            let index = index
            let andressLine = bankFacility.getAndressLine()
            let workTime = bankFacility.getWorkTime()
            let currency = bankFacility.getCurrency()
            let townName = bankFacility.getTownName()
            let displayedBankFacility = List.Something.ViewModel.DisplayedBankFacility(index: index,
                                                                                       adressLine: andressLine,
                                                                                       workTime: workTime,
                                                                                       currency: currency,
                                                                                       townName: townName)
            displayedBankFacilities.append(displayedBankFacility)
        }
        let viewModel = List.Something.ViewModel(displayedBankFacilities: displayedBankFacilities,
                                                 section: countRowAndSection(response.bankFacility))
        viewController?.displaySomething(viewModel: viewModel)
    }

    private func countRowAndSection(_ sortedBankFacilities: [BankFacility]) -> List.Something.ViewModel.Section {
        var cellsInSection = [0]
        var titlesSection: [String] = []
        var section = 0
        var index = 1
        while index < sortedBankFacilities.count {
            if sortedBankFacilities[index - 1].getTownName() != sortedBankFacilities[index].getTownName() {
                cellsInSection[section] += 1
                cellsInSection.append(0)
                titlesSection.append(sortedBankFacilities[index - 1].getTownName())
                section += 1
            } else {
                cellsInSection[section] += 1
            }
            index += 1
        }
        if cellsInSection != [0] {
            cellsInSection[section] += 1
        } else {
            cellsInSection = []
        }
        return List.Something.ViewModel.Section(cellsInSection: cellsInSection, titlesSection: titlesSection)
    }
}
