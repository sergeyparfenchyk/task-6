//
//  ConstantColor.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import UIKit

struct ConstantColor {
    static let background = UIColor.systemBackground
}
