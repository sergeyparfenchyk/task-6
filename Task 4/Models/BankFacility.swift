//
//  BankFacility.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 12.03.22.
//

import Foundation
import CoreLocation

protocol BankFacility: AnyObject {
    func getType() -> TypeBankFacility
    func getCoordinate2D() -> CLLocationCoordinate2D
    func getAndressLine() -> String
    func getWorkTime() -> String
    func getCurrency() -> String
    func getDetails() -> [DetailsBankFacility]
    func getCashIn() -> String
    func getTownName() -> String
}

struct DetailsBankFacility {
    let title: String
    let description: String
}

enum TypeBankFacility: String {
    case infobox = "Инфокиоски"
    case branch = "Подразделения банка"
    case atm = "Банкоматы"
}
