//
//  infoBoxResponse.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation
import CoreLocation

// MARK: - InfoboxResponse
class InfoBoxResponse: Codable {
    var data = InfoBoxResponse.DataClass.init(infobox: [])

    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }

    // MARK: getURL
    static func getUrl() -> URL? {
        return URL(string: "https://belarusbank.by/api/infobox")
    }

    // MARK: - DataClass
    struct DataClass: Codable {
        var infobox: [Infobox]

        enum CodingKeys: String, CodingKey {
            case infobox = "Infobox"
        }

        // MARK: - Infobox
        class Infobox: Codable {
            let infoID: Int
            let countrySubDivision: String
            let townType: String
            let townName: String
            let streetType: String
            let streetName: String
            let buildingNumber: String
            let addressLine: String
            let addressLineDescription: String
            let workTime: String
            let timeLong: String
            let latitude: String
            let longitude: String
            let currency: String
            let type: String
            let cashIn: String
            let cashInCheck: String
            let typeCashIn: String
            let infPrinter: String
            let regionPlatej: String
            let popolneniePlatej: String
            let currentStatus: String
            enum CodingKeys: String, CodingKey {
                case infoID = "info_id"
                case countrySubDivision = "area"
                case townType = "city_type"
                case townName = "city"
                case streetType = "address_type"
                case streetName = "address"
                case buildingNumber = "house"
                case addressLine = "install_place"
                case addressLineDescription = "location_name_desc"
                case workTime = "work_time"
                case timeLong = "time_long"
                case latitude = "gps_x"
                case longitude = "gps_y"
                case currency = "currency"
                case type = "inf_type"
                case cashIn = "cash_in_exist"
                case cashInCheck = "cash_in"
                case typeCashIn = "type_cash_in"
                case infPrinter = "inf_printer"
                case regionPlatej = "region_platej"
                case popolneniePlatej = "popolnenie_platej"
                case currentStatus = "inf_status"
            }
        }
    }
}
