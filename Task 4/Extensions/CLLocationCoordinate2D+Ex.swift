//
//  CLLocationCoordinate2D+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 6.03.22.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    static func getGeographicCoordinates(_ latitude: String, _ longitude: String) -> CLLocationCoordinate2D? {
        if let latitude = Double(latitude),
           let longitude = Double(longitude) {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return nil
    }
}
