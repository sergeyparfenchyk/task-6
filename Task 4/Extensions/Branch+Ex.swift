//
//  Branch+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation
import CoreLocation

extension BranchResponse.DataClass.Branch {

    // MARK: Private Method
    private func getCBU() -> String {
        if let cbu = cbu {
            return cbu
        }
        return ""
    }

    private func getAccountNumber() -> String {
        if let accountNumber = accountNumber {
            return accountNumber
        }
        return ""
    }

    private func createWorkingHoursList() -> String {
        let days = self.information.availability.standardAvailability.day
            let daysString = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
            var workingHoursList: [String] = []
            for day in days {
                var tmpStr = ""
                tmpStr += "\(day.openingTime.dropLast(9))-\(day.closingTime.dropLast(9))"
                if day.dayBreak.breakFromTime.dropLast(9) != day.dayBreak.breakToTime.dropLast(9) {
                    tmpStr += " Перерыв \(day.dayBreak.breakFromTime.dropLast(9))-" +
                    "\(day.dayBreak.breakToTime.dropLast(9))"
                }
                workingHoursList.append(tmpStr)
            }
            var index = 1
            var tmpDay = "\(daysString[0])"
            var tmpWorkingHours = "\(daysString[0])"
            while index < workingHoursList.count {
                if workingHoursList[index - 1] != workingHoursList[index] {
                    if tmpDay != daysString[index - 1] {
                        tmpWorkingHours +=
                        "-\(daysString[index - 1]) \(workingHoursList[index - 1]) \(daysString[index])"
                        tmpDay = daysString[index]
                    } else {
                        tmpWorkingHours += " \(workingHoursList[index - 1]) \(daysString[index])"
                    }
                }
                index += 1
            }
            if tmpDay != daysString[index - 1] {
                tmpWorkingHours += "-\(daysString[index - 1]) \(workingHoursList[index - 1])"
            } else {
                tmpWorkingHours += " \(workingHoursList[index - 1])"
            }
            return tmpWorkingHours
    }
}

// MARK: All details
extension BranchResponse.DataClass.Branch {
//
}

// MARK: - BankFacility
extension BranchResponse.DataClass.Branch: BankFacility {
    func getType() -> TypeBankFacility {
        .branch
    }

    func getCoordinate2D() -> CLLocationCoordinate2D {
        if let latitude = Double(address.geoLocation.geographicCoordinates.latitude),
           let longitude = Double(address.geoLocation.geographicCoordinates.longitude) {
            let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            return locationCoordinate
        }
        return CLLocationCoordinate2D()
    }

    func getAndressLine() -> String {
        return address.addressLine
    }

    // MARK: need FIX
    func getWorkTime() -> String {
        return createWorkingHoursList()
    }

    func getCurrency() -> String {
        return ""
    }

    func getCashIn() -> String {
        return "-"
    }

    func getTownName() -> String {
        return address.townName
    }

    // MARK: need FIX
    func getDetails() -> [DetailsBankFacility] {
        let details = [DetailsBankFacility(title: "Уникальный идентификатор",
                                           description: branchID),
                       DetailsBankFacility(title: "Наименование отделения",
                                           description: name),
                       DetailsBankFacility(title: "Номер ЦБУ. Отображается только для ЦБУ",
                                           description: getCBU()),
                       DetailsBankFacility(title: "Номер рассчетного счета ЦБУ",
                                           description: getAccountNumber()),
                       DetailsBankFacility(title: "Наличие Wi-Fi",
                                           description: wifi == 0 ? ConstantString.not : ConstantString.yes),
                       DetailsBankFacility(title: "Наличие электронной очереди",
                                           description: equeue == 0 ? ConstantString.not : ConstantString.yes),
                       DetailsBankFacility(title: "Название улицы или проспекта",
                                           description: address.streetName),
                       DetailsBankFacility(title: "Номер здания",
                                           description: address.buildingNumber),
                       DetailsBankFacility(title: "Номер корпуса здания",
                                           description: address.department),
                       DetailsBankFacility(title: "Почтовый индекс",
                                           description: address.postCode),
                       DetailsBankFacility(title: "Название населенного пункта",
                                           description: address.townName),
                       DetailsBankFacility(title: "Название региона страны",
                                           description: address.countrySubDivision),
                       DetailsBankFacility(title: "Широта",
                                           description: address.geoLocation.geographicCoordinates.latitude),
                       DetailsBankFacility(title: "Долгота",
                                           description: address.geoLocation.geographicCoordinates.longitude),
                       DetailsBankFacility(title: "Режим работы",
                                           description: createWorkingHoursList())]
        return details
    }
}
