//
//  Infobox+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 7.03.22.
//

import Foundation
import CoreLocation

// MARK: - BankFacility
extension InfoBoxResponse.DataClass.Infobox: BankFacility {
    func getType() -> TypeBankFacility {
        .infobox
    }

    func getCoordinate2D() -> CLLocationCoordinate2D {
        if let latitude = Double(latitude),
           let longitude = Double(longitude) {
            let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            return locationCoordinate
        }
        return CLLocationCoordinate2D()
    }

    func getAndressLine() -> String {
        return addressLine
    }

    func getWorkTime() -> String {
        return workTime
    }

    func getCurrency() -> String {
        return currency
    }

    func getCashIn() -> String {
        return cashInCheck
    }

    func getTownName() -> String {
        return townName
    }

    // MARK: need FIX
    func getDetails() -> [DetailsBankFacility] {
        let details = [DetailsBankFacility(title: "Уникальный идентификатор",
                                           description: String(infoID)),
                       DetailsBankFacility(title: "Область",
                                           description: countrySubDivision),
                       DetailsBankFacility(title: "Населённый пункта",
                                           description: townType + " " + townName),
                       DetailsBankFacility(title: "Улица",
                                           description: streetType + " " + streetName),
                       DetailsBankFacility(title: "Дом",
                                           description: buildingNumber),
                       DetailsBankFacility(title: "Место установки",
                                           description: addressLine),
                       DetailsBankFacility(title: "Режим работы инфокиоска",
                                           description: workTime),
                       DetailsBankFacility(title: "Координата широты",
                                           description: latitude),
                       DetailsBankFacility(title: "Координата долготы",
                                           description: longitude),
                       DetailsBankFacility(title: "Перечень валют с которыми работает инфокиоск",
                                           description: currency),
                       DetailsBankFacility(title: "Тип инфокиоска",
                                           description: type),
                       DetailsBankFacility(title: "Наличие купюроприемника",
                                           description: cashIn),
                       DetailsBankFacility(title: "Исправность купюроприемника",
                                           description: cashInCheck),
                       DetailsBankFacility(title: "Приёмник пачек банкнот",
                                           description: typeCashIn),
                       DetailsBankFacility(title: "Возможность печати чека",
                                           description: infPrinter),
                       DetailsBankFacility(title: "Наличие платежа \"Региональные платежи\"",
                                           description: regionPlatej),
                       DetailsBankFacility(title: "Наличие платежа \"Пополнение картсчета наличными\"",
                                           description: popolneniePlatej),
                       DetailsBankFacility(title: "Исправность инфокиоска",
                                           description: currentStatus)]
        return details
    }
}
