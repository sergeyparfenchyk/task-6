//
//  ATM+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import Foundation
import CoreLocation

extension ATMResponse.DataClass.ATM {
    private func createWorkingHoursList() -> String {
        let days = self.availability.standardAvailability.day
        let daysString = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
        var workingHoursList: [String] = []
        for day in days {
            var tmpStr = ""
            tmpStr += "\(day.openingTime)-\(day.closingTime)"
            if day.dayBreak.breakFromTime != day.dayBreak.breakToTime {
                tmpStr += " Перерыв \(day.dayBreak.breakFromTime)-\(day.dayBreak.breakToTime)"
            }
            workingHoursList.append(tmpStr)
        }
        var index = 1
        var tmpDay = "\(daysString[0])"
        var tmpWorkingHours = "\(daysString[0])"
        while index < workingHoursList.count {
            if workingHoursList[index - 1] != workingHoursList[index] {
                if tmpDay != daysString[index - 1] {
                    tmpWorkingHours +=
                    "-\(daysString[index - 1]) \(workingHoursList[index - 1]) \(daysString[index])"
                    tmpDay = daysString[index]
                } else {
                    tmpWorkingHours += " \(workingHoursList[index - 1]) \(daysString[index])"
                }
            }
            index += 1
        }
        if tmpDay != daysString[index - 1] {
            tmpWorkingHours += "-\(daysString[index - 1]) \(workingHoursList[index - 1])"
        } else {
            tmpWorkingHours += " \(workingHoursList[index - 1])"
        }
        return tmpWorkingHours
    }

    private func isCachIn() -> Bool {
        let services = self.services
        for service in services where service.serviceType == "CashIn" {
            return true
        }
        return false
    }

    private func getCards() -> String {
        var allCards = ""
        for card in cards {
            allCards += "\(card) "
        }
        return allCards
    }

    private func getCurrentStatus() -> String {
        switch currentStatus {
        case ConstantStringCheck.currentStatusOn:
            return ConstantString.currentStatusOn
        case ConstantStringCheck.currentStatusOff:
            return ConstantString.currentStatusOff
        case ConstantStringCheck.currentStatusTempOff:
            return ConstantString.currentStatusTempOff
        default:
            break
        }
        return ""
    }

    private func getCordinate() -> String {
        return "\(address.geolocation.geographicCoordinates.latitude)," +
        "\(address.geolocation.geographicCoordinates.longitude)"
    }

    private func getAddress() -> String {
        var fullAdress = ""
        if address.streetName != "" {
            fullAdress += "\(address.streetName), "
        }
        if address.buildingNumber != "" {
            fullAdress += "\(address.buildingNumber), "
        }
        if address.townName != "" {
            fullAdress += "\(address.townName), "
        }
        if address.countrySubDivision != "" {
            if address.countrySubDivision != "Минск" {
                fullAdress += "\(address.countrySubDivision) обл. "
            }
        }
        return fullAdress
    }

    private func checkService(_ serviceType: String) -> String {
        for service in services where service.serviceType == serviceType {
            if service.serviceDescription == "" {
                return ConstantString.yes
            } else {
                return service.serviceDescription
            }
        }
        return ConstantString.not
    }
}

// MARK: - BankFacility
extension ATMResponse.DataClass.ATM: BankFacility {
    func getType() -> TypeBankFacility {
        .atm
    }

    func getCoordinate2D() -> CLLocationCoordinate2D {
        if let latitude = Double(address.geolocation.geographicCoordinates.latitude),
           let longitude = Double(address.geolocation.geographicCoordinates.longitude) {
            let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            return locationCoordinate
        }
        return CLLocationCoordinate2D()
    }

    func getAndressLine() -> String {
        return address.addressLine
    }

    // MARK: need FIX
    func getWorkTime() -> String {
        return createWorkingHoursList()
    }

    func getCurrency() -> String {
        return currency
    }

    func getCashIn() -> String {
        return checkService(ConstantStringCheck.serviceTypeCashIn)
    }

    func getTownName() -> String {
        return address.townName
    }

    // swiftlint:disable all
    func getDetails() -> [DetailsBankFacility] {
        let details = [DetailsBankFacility(title: ConstantString.atmId,
                                           description: atmID),
                       DetailsBankFacility(title: ConstantString.baseCurrency,
                                           description: baseCurrency),
                       DetailsBankFacility(title: ConstantString.currency,
                                           description: currency),
                       DetailsBankFacility(title: ConstantString.cards,
                                           description: getCards()),
                       DetailsBankFacility(title: ConstantString.currentStatus,
                                           description: getCurrentStatus()),
                       DetailsBankFacility(title: ConstantString.address,
                                           description: getAddress()),
                       DetailsBankFacility(title: ConstantString.addressLine,
                                           description: address.addressLine),
                       DetailsBankFacility(title: ConstantString.addresDescription,
                                           description: address.addressDescription),
                       DetailsBankFacility(title: ConstantString.сoordinates,
                                           description: getCordinate()),
                       DetailsBankFacility(title: ConstantString.isRestricted,
                                           description: availability.isRestricted ? ConstantString.yes : ConstantString.not),
                       DetailsBankFacility(title: ConstantString.availability,
                                           description: createWorkingHoursList()),
                       DetailsBankFacility(title: ConstantString.sameAsOrganization,
                                           description: availability.sameAsOrganization ? ConstantString.yes : ConstantString.not),
                       DetailsBankFacility(title: ConstantString.phoneNumber,
                                           description: contactDetails.phoneNumber),
                       DetailsBankFacility(title: ConstantString.serviceTypeCashWithdrawal,
                                           description: checkService(ConstantStringCheck.serviceTypeCashWithdrawal)),
                       DetailsBankFacility(title: ConstantString.serviceTypePinChange,
                                           description: checkService(ConstantStringCheck.serviceTypePinChange)),
                       DetailsBankFacility(title: ConstantString.serviceTypePINUnblock,
                                           description: checkService(ConstantStringCheck.serviceTypePINUnblock)),
                       DetailsBankFacility(title: ConstantString.serviceTypePINActivation,
                                           description: checkService(ConstantStringCheck.serviceTypePINActivation)),
                       DetailsBankFacility(title: ConstantString.serviceTypeBalance,
                                           description: checkService(ConstantStringCheck.serviceTypeBalance)),
                       DetailsBankFacility(title: ConstantString.serviceTypeMiniStatement,
                                           description: checkService(ConstantStringCheck.serviceTypeMiniStatement)),
                       DetailsBankFacility(title: ConstantString.serviceTypeBillPayments,
                                           description: checkService(ConstantStringCheck.serviceTypeBillPayments)),
                       DetailsBankFacility(title: ConstantString.serviceTypeMobileBankingRegistration,
                                           description: checkService(ConstantStringCheck.serviceTypeMobileBanking)),
                       DetailsBankFacility(title: ConstantString.serviceTypeCurrencyExhange,
                                           description: checkService(ConstantStringCheck.serviceTypeCurrencyExhange)),
                       DetailsBankFacility(title: ConstantString.serviceTypeCashIn,
                                           description: checkService(ConstantStringCheck.serviceTypeCashIn)),
                       DetailsBankFacility(title: ConstantString.serviceTypeOther,
                                           description: checkService(ConstantStringCheck.serviceTypeOther))]
        return details
    }
}
